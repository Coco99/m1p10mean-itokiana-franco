export class CarDeposit
{
    owner: string ;
    carModel: string ;
    carPicture : string; 
    dateDeposit : Date ;
    responsableReception: string ;

    constructor( carModel: string , carPicture : string )
    {
        this.carModel = carModel ;
        this.carPicture = carPicture ;
    }

}