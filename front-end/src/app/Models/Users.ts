export class Users
{
    id : string ;
    firstname : string ;
    name : string ;
    email : string ;
    password : string ;
    role : string ;
    
    constructor( firstname : string , name : string , email : string , password : string )
    {
        this.firstname = firstname ;
        this.name = name ;
        this.email = email ;
        this.password = password ;
    }
} 