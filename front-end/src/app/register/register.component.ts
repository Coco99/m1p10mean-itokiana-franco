import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup , Validators } from '@angular/forms'; 
import { Title } from '@angular/platform-browser';
import { Users } from '../Models/Users';
import { UserServiceService } from '../services/user-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit
{
  message:string; 
  hidden_message : boolean;

  register_form : FormGroup ; 

  name : FormControl ;
  firstname : FormControl ;
  email : FormControl ;
  password : FormControl ;

  constructor( private titleService: Title , private userService : UserServiceService )
  {
    this.titleService.setTitle("S' inscrire à TC-Garage");
  }

  new_user : Users;

  ngOnInit(): void 
  {
    this.name = new FormControl('', Validators.required);
    this.firstname = new FormControl('', Validators.required);
    this.email = new FormControl('', [
      Validators.required,
      Validators.pattern("[a-zA-z0-9]+@[a-zA-z0-9]+.[a-zA-z0-9]{2,4}") 
    ]);
    this.password = new FormControl('', [
      Validators.required,
      Validators.minLength(8)
    ]);

    this.register_form = new FormGroup({
      name : this.name , 
      firstname : this.firstname , 
      email : this.email ,
      password : this.password ,
    });
    this.hidden_message = true;
  }

  register()
  {
    this.new_user = new Users(
      this.register_form.value.name ,
      this.register_form.value.firstname ,
      this.register_form.value.email ,
      this.register_form.value.password
    );

    this.userService.register_client( this.new_user ).subscribe((response: any) => {
      
      this.hidden_message = false;
      this.message = "Utilisateur inscrit avec succès";
      
      // console.log(response.message);
    });
    
    this.register_form.reset();
  }
}
