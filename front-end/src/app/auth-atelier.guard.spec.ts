import { TestBed } from '@angular/core/testing';

import { AuthAtelierGuard } from './auth-atelier.guard';

describe('AuthAtelierGuard', () => {
  let guard: AuthAtelierGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AuthAtelierGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
