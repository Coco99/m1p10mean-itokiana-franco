import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit
{
  // icons
  fa_bars = faBars;

  constructor( private service_auth : AuthService , private router: Router )
  {

  }

  ngOnInit(): void 
  {
   
  }

  get_role()
  {
    return this.service_auth.getRole() ;
  }

  

  log_out()
  {
    this.service_auth.logout();
    this.router.navigate(['/login']);
  }
}
