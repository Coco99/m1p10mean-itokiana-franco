import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit
{
  constructor( private titleService : Title )
  {
    this.titleService.setTitle("Liste des utilisateurs");
  }

  ngOnInit(): void 
  {
    
  }
}
