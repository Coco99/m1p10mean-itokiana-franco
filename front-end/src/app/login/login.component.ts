import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup , Validators } from '@angular/forms'; 
import { Title } from '@angular/platform-browser';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
}) 
export class LoginComponent implements OnInit
{
  message_error: string ;
  hidden_message_error : boolean;
  
  login_form : FormGroup;
  email : FormControl; 
  password : FormControl;

  constructor(private service_auth : AuthService , 
              private titleService:Title )
  {
    this.titleService.setTitle("Se connecter à TC-Garage");
  }

  ngOnInit(): void 
  {
    // localStorage.setItem("STATE" , "false");
    localStorage.setItem("ROLE" , "null");

    this.email = new FormControl( 'ramarotafika1999@gmail.com', Validators.required );
    this.password = new FormControl( '123456789', Validators.required );

    this.login_form = new FormGroup({
      email : this.email ,
      password : this.password ,
    });
    this.hidden_message_error = true ;
  }

  login()
  {
    this.service_auth.log_user( this.login_form.value.email , this.login_form.value.password );

    if ( localStorage.getItem( "STATE" ) == "false"  )
    {
      this.hidden_message_error = false ;
      this.message_error = "Utilisateur non trouvé";
      this.login_form.reset();
    }
    else
    {
      this.login_form.reset();
    }
  }
}
 