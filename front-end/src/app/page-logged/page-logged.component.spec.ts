import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageLoggedComponent } from './page-logged.component';

describe('PageLoggedComponent', () => {
  let component: PageLoggedComponent;
  let fixture: ComponentFixture<PageLoggedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageLoggedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PageLoggedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
