import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-page-logged',
  templateUrl: './page-logged.component.html',
  styleUrls: ['./page-logged.component.css']
})
export class PageLoggedComponent implements OnInit 
{
  constructor( private titleService : Title )
  {
    this.titleService.setTitle("Vous êtes bien connecté");
  }

  ngOnInit(): void {
    
  }
}
