import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { url_prod } from 'src/environments/environment';
import { Users } from '../Models/Users';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
 
  constructor( private http : HttpClient ) { }

  // inscription client
  register_client( user: Users )
  {
    return this.http.post( url_prod + "client/register" , user );
  }

  // connextion client
  login( email:string , password:string  )
  {
    return this.http.post( url_prod + "client/login" , { email , password } );
  }

  login_atelier( email:string , password:string  )
  {
    return this.http.post( url_prod + "atelier/login" , { email , password } );
  }

  // détails client connecté
  get_user( user_logged_token : string )
  {
    const headers_ = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + user_logged_token
    })
    return this.http.get<Users>( url_prod + "client/identity" , { headers : headers_ });
  }
}

