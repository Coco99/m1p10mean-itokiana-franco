import { Injectable } from '@angular/core';
import { UserServiceService } from './user-service.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService 
{
  isLogin = false;
  roleAs: string;

  constructor(  private userService : UserServiceService , private router : Router) { }

  // login() {
  //   this.isLogin = true;
  //   this.roleAs = "ADMIN";
  //   localStorage.setItem('STATE', 'true');
  //   localStorage.setItem('ROLE', this.roleAs);
  //   return ({ success: this.isLogin, role: this.roleAs });
  // }


  log_user( email:string , password : string )
  {
    this.isLogin = true;

    this.userService.login(  email , password  ).subscribe(
      (data : any ) => { 
      localStorage.setItem( "login_error" , "" );
      localStorage.setItem( "auth_token" , "" );
      localStorage.setItem( "auth_token" , data.data.token );
    },
    (error) => {
      localStorage.setItem( "login_error" , error.error.errorMessage );
      // console.log(error.error.errorMessage);      
    });

    // console.log( localStorage.getItem( "auth_token") );

    this.userService.get_user( localStorage.getItem( "auth_token") || '{}' ).subscribe(
      (response: any) => { 
      // console.log(response.data.role);
      localStorage.setItem('STATE' , 'false');
      localStorage.setItem('STATE' , 'true');
      localStorage.setItem('ROLE' , 'null' );
      localStorage.setItem('ROLE', response.data.role );

      this.router.navigate(['/home']);
    },
    (error) => {
      localStorage.setItem('ROLE' , 'null' );
      // console.log(error.error.errorMessage);      
    });
    
    this.roleAs = localStorage.getItem('ROLE') || '{}';
    return ({ success: this.isLogin, role: this.roleAs });
  }

  log_user_atelier( email:string , password : string )
  {
    this.isLogin = true;

    this.userService.login_atelier(  email , password  ).subscribe(
      (data : any ) => { 
      localStorage.setItem( "login_error" , "" );
      localStorage.setItem( "auth_token" , "" );
      localStorage.setItem( "auth_token" , data.data.token );

      localStorage.setItem('STATE' , 'false');
      localStorage.setItem('STATE' , 'true');
      localStorage.setItem('ROLE' , 'null' );
      localStorage.setItem('ROLE', 'respAtelier' );

    },
    (error) => {
      localStorage.setItem( "login_error" , error.error.errorMessage );
      // console.log(error.error.errorMessage);      
    });      

    this.router.navigate(['/home']);
      
    this.roleAs = localStorage.getItem('ROLE') || '{}';
    return ({ success: this.isLogin, role: this.roleAs });
  }
  


  logout() 
  {
    this.isLogin = false;
    this.roleAs = '';
    localStorage.setItem('STATE', 'false');
    localStorage.setItem('ROLE', 'null');
    localStorage.setItem( "auth_token" , "" );
    return ({ success: this.isLogin, role: 'null' });
  }

  isLoggedIn() 
  {
    const loggedIn = localStorage.getItem('STATE');
    if (loggedIn === 'true')
      this.isLogin = true;
    else
      this.isLogin = false;
    return this.isLogin;
  }

  getRole() 
  {
    return localStorage.getItem('ROLE') || '{}' ; 
  }

  
}
