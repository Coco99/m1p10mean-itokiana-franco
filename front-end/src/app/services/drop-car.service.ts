import { HttpClient , HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CarDeposit } from '../Models/CarDeposit';
import { url_prod } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DropCarService {

  constructor(private http : HttpClient) 
  { 

  }

  //  Déposer car
  drop_off_car( user_logged_token : string , car_deposit : CarDeposit )
  {
    const headers_ = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + user_logged_token
    })
    return this.http.post( url_prod + "client/cars/dropoff" , car_deposit ,  { headers : headers_ }  );
  }

  // liste des voitures en dépôt
  get_all_deposit( user_logged_token : string )
  {
    const headers_ = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + user_logged_token
    })
    return this.http.get<CarDeposit[]>( url_prod + "client/cars" , { headers : headers_ });
  }
}
