import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CarDeposit } from '../Models/CarDeposit';
import { url_prod } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CarsService { 

  constructor( private http : HttpClient ) 
  {

  }

  // getCars()
  // {
  //   return this.http.get<Cars[]>( url_domaine + "cars/dropoff" );
  // } 
  

  //   Déposer car
  drop_off_car( user_logged_token : string , car_deposit : CarDeposit )
  {
    const headers_ = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + user_logged_token
    })
    return this.http.post( url_prod + "client/cars/dropoff" , { headers : headers_ , car_deposit });
  }
}
