import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule , Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { NavigationComponent } from './template/navigation/navigation.component';
import { AsideComponent } from './template/aside/aside.component';
import { RegisterComponent } from './register/register.component';
import { CarsComponent } from './cars/cars.component';
import { LoaderComponent } from './template/loader/loader.component';
import { NgxSpinnerModule } from 'ngx-spinner';  
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { StatsComponent } from './stats/stats.component';
import { AuthGuard } from './auth.guard';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TopbarComponent } from './template/topbar/topbar.component';
import { DepotComponent } from './cars/depot/depot.component';
import { PageLoggedComponent } from './page-logged/page-logged.component';
import { ListUserComponent } from './list-user/list-user.component';
import { ListByUserComponent } from './cars/depot/list-by-user/list-by-user.component';
import { ListAtelierComponent } from './cars/depot/list-atelier/list-atelier.component';
import { AtelierComponent } from './login/atelier/atelier.component';

const routes_ : Routes = [
  { path : "login" , component : LoginComponent },
  { path : "login_atelier" , component : AtelierComponent },
  { path : "register" , component : RegisterComponent },
  { path : "home" , 
    component : PageLoggedComponent ,
    canActivate: [AuthGuard] ,
    data: {
      role: 'client' || 'respAtelier'
    }
  },
  { path : "depot_car" , 
    component : DepotComponent , 
    canActivate: [AuthGuard] ,
    data: {
      role: 'client'
    }
  },
  { path : "list_depot_car" , 
    component : ListByUserComponent , 
    canActivate: [AuthGuard] ,
    data: {
      role: 'client'
    }
  },
  { path : "list_user" , 
    component : ListUserComponent ,
    canActivate: [AuthGuard] ,
    data: {
      role: 'respAtelier'
    }
  },
  { path : "cars" , 
    component : CarsComponent ,
    canActivate: [AuthGuard] ,
    data: {
      role: ''
    }
  },
  { path : "stats" , 
    component : StatsComponent ,
    canActivate: [AuthGuard] ,
    data: {
      role: ''
    }
  },
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavigationComponent,
    AsideComponent,
    RegisterComponent,
    CarsComponent,
    LoaderComponent,
    StatsComponent,
    TopbarComponent,
    DepotComponent,
    PageLoggedComponent,
    ListUserComponent,
    ListByUserComponent,
    ListAtelierComponent,
    AtelierComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes_),
    NgxSpinnerModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    FormsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FontAwesomeModule
  ],
  providers: [
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
