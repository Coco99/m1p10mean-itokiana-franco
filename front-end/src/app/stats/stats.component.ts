import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit
{
  public options: any = {   
    chart: {
       type: 'column'
    },
    title: {
       text: 'title'
    },
    subtitle:{
       text: 'subtitle' 
    },
    xAxis:{
       categories: [],
       crosshair: true        
    },     
    yAxis : {
       min: 0,
       title: {
          text: null         
       }      
    },
    plotOptions : {
       column: {
          pointPadding: 0.2,
          borderWidth: 0
       }
    },
    series: [
    //   {
    //     name: 'Tokyo',
    //     data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6,
    //         148.5, 216.4, 194.1, 95.6, 54.4]
    //   }, 
    //   {
    //     name: 'New York',
    //     data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3,
    //         91.2, 83.5, 106.6, 92.3]
    //   }, {
    //     name: 'London',
    //     data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6,
    //       52.4, 65.2, 59.3, 51.2]
    // }, 
    {
        name: 'Berlin',
        data: [5]
    },
    {
      name: 'Etat-Unis',
      data: [10]
  },
  ]
};
  constructor()
  {

  }

  ngOnInit(): void 
  {
    Highcharts.chart('container', this.options);
  }
}
