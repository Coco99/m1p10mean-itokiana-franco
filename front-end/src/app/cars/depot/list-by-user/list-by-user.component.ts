import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { CarDeposit } from 'src/app/Models/CarDeposit';
import { DropCarService } from 'src/app/services/drop-car.service';

@Component({
  selector: 'app-list-by-user', 
  templateUrl: './list-by-user.component.html',
  styleUrls: ['./list-by-user.component.css']
})
export class ListByUserComponent implements OnInit
{
  all_car_dropped : CarDeposit[];

  recherche : any ;
  page_number: number = 1; 

  constructor(  private titleService : Title , 
                private drop_car_service : DropCarService ,
                private SpinnerService: NgxSpinnerService  )
  {
    this.titleService.setTitle("Liste des voitures en dépôt");
  }

  ngOnInit(): void 
  { 
    this.SpinnerService.show(); 

    const token_auth : string = localStorage.getItem("auth_token") || '{}';

    this.drop_car_service.get_all_deposit( token_auth ).subscribe(
      ( data : any ) =>{
        this.all_car_dropped = data.data;
        this.SpinnerService.hide();
      }
    );
  }
}
