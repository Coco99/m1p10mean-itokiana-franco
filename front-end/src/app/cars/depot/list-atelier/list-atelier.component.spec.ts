import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAtelierComponent } from './list-atelier.component';

describe('ListAtelierComponent', () => {
  let component: ListAtelierComponent;
  let fixture: ComponentFixture<ListAtelierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListAtelierComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListAtelierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
