import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { CarDeposit } from 'src/app/Models/CarDeposit';
import { DropCarService } from 'src/app/services/drop-car.service';
import { UserServiceService } from 'src/app/services/user-service.service';

@Component({
  selector: 'app-depot',
  templateUrl: './depot.component.html',
  styleUrls: ['./depot.component.css']
})
export class DepotComponent implements OnInit
{
  drop_car_form : FormGroup ;
  new_drop_car : CarDeposit ;

  hidden_message : boolean ;
  message : string = "La voiture a été déposée avec succès" ;
  
  carModel : FormControl ;
  carPicture : FormControl ;

  constructor( private titleService : Title , 
    private drop_car_service : DropCarService , 
    private user_service : UserServiceService )
  {
    this.titleService.setTitle("Déposer une voiture");
  }

  ngOnInit(): void 
  {
    this.hidden_message = true ;
    this.carModel = new FormControl('', Validators.required);
    this.carPicture = new FormControl('', Validators.required); 

    this.drop_car_form = new FormGroup({
      carModel : this.carModel , 
      carPicture : this.carPicture
    }); 
  }

  drop_car()
  {    
    const token_auth : string = localStorage.getItem("auth_token")|| '{}';
    // console.log(token_auth);
      
    this.new_drop_car = new CarDeposit(
      this.drop_car_form.value.carModel , 
      this.drop_car_form.value.carPicture
    );

    
    this.drop_car_service.drop_off_car( token_auth , this.new_drop_car )
    .subscribe( (data: any) => {
      this.hidden_message = false;
      // console.log(data.message);
    },
    error => {
      console.log(error);
    });
    this.drop_car_form.reset();
  }
}
 