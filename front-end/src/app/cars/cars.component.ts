import { Component, OnInit } from '@angular/core';
import { CarsService } from '../services/cars.service';
import { NgxSpinnerService } from "ngx-spinner"; 

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit
{

  recherche : any ;
  // cars : Cars[];
  page_number: number = 1; 

  constructor(  private cars_service : CarsService , 
                private SpinnerService: NgxSpinnerService , )
  {

  }

  ngOnInit(): void 
  {
    // this.SpinnerService.show(); 

    // this.cars_service.getCars().subscribe(
    //   (data: Cars[]) => {
    //     this.cars = data;
    //     this.SpinnerService.hide(); 
    //   });
  }
}
