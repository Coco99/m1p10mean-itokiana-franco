const { Schema, model } = require("mongoose");

const repairModel = new Schema({
  depositCar: {
    type: Schema.Types.ObjectId,
    ref: "CarDeposit",
    required: true,
  },
  repairDescription: {
    type: Schema.Types.String,
    required: true,
  },
  dateDebutRepair: {
    type: Schema.Types.Date,
    default: Date.now(),
  },
  avancement: {
    type: Schema.Types.Number,
    default: 0,
  },
  prix: {
    type: Schema.Types.Number,
    required: true,
  },
});

module.exports = model("RepairsCars", repairModel);
