const { Schema, model } = require("mongoose");

const carDepositModel = new Schema({
  owner: {
    type: Schema.Types.ObjectId,
    ref: "UsersApp",
    required: true,
  },
  carModel: {
    type: String,
    required: true,
  },
  carPicture: String,
  dateDeposit: {
    type: Schema.Types.Date,
    default: Date.now(),
  },
  responsableReception: {
    type: Schema.Types.ObjectId,
    required: false,
    default: null,
  },
});

module.exports = model("CarDeposit", carDepositModel);
