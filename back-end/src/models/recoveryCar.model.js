const { Schema, model } = require("mongoose");

const recoveryCarModel = new Schema({
  carDeposit: {
    type: Schema.Types.ObjectId,
    ref: "CarDeposit",
  },
  dateRecovery: {
    type: Schema.Types.Date,
    default: Date.now(),
  },
  responsableRecovery: {
    type: Schema.Types.ObjectId,
    ref: "UsersApp",
    default: null,
  },
});

module.exports = model("RecoveryData", recoveryCarModel);
