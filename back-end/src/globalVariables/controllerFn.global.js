const { createUser, getUserByEmail } = require("./helperFn.global");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const statusCode = require("./statusCode");

const getToken = async (req, res, next) => {
  try {
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      throw new Error("Token non valide ou absent");
    }
    const token = authHeader.split(" ")[1];
    const user = jwt.verify(token, process.env.SECRET_KEY_JWT);
    if (!user) {
      throw new Error("Le token ne correspond a aucun utilisateur");
    }
    req.user = user;
    next();
  } catch (error) {
    next(error);
  }
};

// fonction pour avoir les informations de l'utilisateur connecte
const identity = async (req, res, next) => {
  const user = req.user;

  res.status(200).json({
    data: user,
  });
};

const authLogin = async (req, res, next) => {
  const { email, password } = req.body;
  const role = req.role;
  try {
    const allClientByEmail = await getUserByEmail({ email, role });
    const matchingClients = await Promise.all(
      allClientByEmail.map(async (client) => {
        const match = await bcrypt.compare(password, client.password);
        return match ? client : undefined;
      })
    );
    const userLogged = matchingClients.find((client) => client);

    if (userLogged) {
      const token = await jwt.sign(
        {
          _id: userLogged._id,
          firstname: userLogged.firstname,
          name: userLogged.name,
          email: userLogged.email,
          role: userLogged.role,
        },
        process.env.SECRET_KEY_JWT || "secret_key",
        { expiresIn: "12h" }
      );

      res.status(200).json({
        message: "User logged successfully",
        data: {
          token,
        },
      });
    } else {
      req.status = statusCode.unauthorized;
      throw new Error("User not found");
    }
  } catch (error) {
    next(error);
  }
};

const authSignIn = async (req, res, next) => {
  const role = req.role;
  const user = req.body;
  try {
    const passwordHashed = await bcrypt.hash(user.password, 10);
    const client = await createUser(
      { ...user, password: passwordHashed },
      role
    );
    res.status(201).json({
      message: "User added succeffully",
      data: client,
    });
  } catch (error) {
    next(error);
  }
};

module.exports = {
  getToken,
  authLogin,
  authSignIn,
  identity,
};
