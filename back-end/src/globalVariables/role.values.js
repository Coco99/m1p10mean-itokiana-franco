const role = {
  responsableAtelier: "respAtelier",
  responsableFinancier: "respFinancier",
  client: "client",
};

module.exports = { role };
