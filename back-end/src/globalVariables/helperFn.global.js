const User = require("../models/user.model");
const createUser = (client, role) => {
  return User.create({
    ...client,
    role: role,
  });
};

const getUserByEmail = ({ email, role }) => {
  return User.find({ role: role }).where("email").equals(email).exec();
};

module.exports = {
  createUser,
  getUserByEmail,
};
