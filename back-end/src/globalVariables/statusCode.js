const statusCode = {
  ok: 200,
  unauthorized: 401,
  notAllowRole: 403,
  badRequest: 400,
};

module.exports = statusCode;
