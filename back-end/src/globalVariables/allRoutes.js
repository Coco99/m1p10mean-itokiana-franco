const allRoutes = [
  {
    route: "/client",
    routesChild: [
      {
        route: "/login",
        body: "{email, password}",
        return: "{message, data:{token} } ",
      },
      {
        route: "/register",
        body: "{firstname, name, email, password} ",
        return: "{message, data: user} ",
      },
      {
        title: "Deposer une voiture",
        route: "/cars/dropoff",
        body: "{model: modelCar, picture: pictureCar}",
        comments: "pictureCar n'est pas obligatoire",
        token: "Auth Bearer",
      },
      {
        title: "Recuperer ue voiture",
        route: "/cars/recovery/:idCar",
        body: "none",
        paramsUrl: "idCar",
        token: "Auth Bearer",
      },
    ],
  },
  {
    route: "/atelier",
    routeChild: [
      {
        route: "/login",
        body: "{email, password}",
        return: "{message, data:{token} } ",
      },
      {
        route: "/register",
        body: "{firstname, name, email, password} ",
        return: "{message, data: user} ",
      },
      {
        title:
          "Receptionner une voiture apres que le client a depose sa voiture",
        route: "/depotCar/:idDepot/reception",
        body: "none",
        paramsUrl: ":idDepot",
        token: "Auth Bearer",
      },
      {
        title: "Ajouter les reparations cote atelier",
        route: "/depotCar/:idDepot/repairs",
        body: "none",
        paramsUrl: ":idDepot",
        token: "Auth Bearer",
      },
    ],
  },
];

module.exports = allRoutes;
