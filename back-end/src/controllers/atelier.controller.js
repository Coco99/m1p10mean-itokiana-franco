const { role } = require("../globalVariables/role.values");
const statusCode = require("../globalVariables/statusCode");
const { findCarById } = require("../helpers/carDeposit.helper");
const { receptionCars } = require("../helpers/reponsableAtelier.helper");
const repairsModel = require("../models/repairs.model");

const receptionVoiture = async (req, res, next) => {
  try {
    const idDepot = req.params.idDepot;
    if (!idDepot && idDepot == "") {
      throw new Error("Aucune voiture n'a ete selectionne");
    }
    const responsableAtelier = req.user;
    if (user.role !== role.responsableAtelier) {
      req.status = statusCode.notAllowRole;
      throw new Erro("Utilisateur non authorise");
    }

    const depositUpdated = await receptionCars(idDepot, responsableAtelier._id);

    if (depositUpdated) {
      res.status(statusCode.ok).json({
        message: "Deposit car's updated successfully",
      });
    }
  } catch (error) {
    next(error);
  }
};

const allReparationsWithDetails = async (req, res, next) => {
  try {
    const user = req.user;
    if (user.role !== role.responsableAtelier) {
      req.status = statusCode.notAllowRole;
      throw new Error("Utilisateur non authorise");
    }
    const idDepot = req.params.idDepot;
    if (!idDepot && idDepot == "") {
      throw new Error("Aucune voiture n'a ete selectionne");
    }
    const depositCar = await findCarById(idDepot);
    if (!depositCar) {
      throw new Error("Aucune voiture ne correspond a cette identifiant");
    }
    const { allReparation } = req.body;
    const allReparationDepositCar = allReparation.map((repair) => ({
      ...repair,
      depositCar: depositCar._id,
    }));
    const allRepairsAdded = await repairsModel.create(allReparationDepositCar);

    if (allRepairsAdded && typeof allRepairsAdded === Array) {
      res.status(statusCode.ok).json({
        message: "All repairs are added",
      });
    } else {
      throw new Error("There's a problem during saving repairs");
    }
  } catch (error) {
    next(error);
  }
};

const setAvancementRepairs = async (req, res, next) => {
  try {
    const user = req.user;
    if (user.role !== role.responsableAtelier) {
      req.status = statusCode.notAllowRole;
      throw new Error("Utilisateur non authorise");
    }
    const idDepot = req.params.idDepot;
    if (!idDepot && idDepot == "") {
      throw new Error("Aucune voiture n'a ete selectionne");
    }
    const depositCar = await findCarById(idDepot);
    if (!depositCar) {
      throw new Error("Aucune voiture ne correspond a cet identifiant");
    }
    const idRepair = req.params.idRepair;
    if (!idRepair && idRepair == "") {
      throw new Error("Aucune reparation n'a ete selectionne");
    }

    const repair = await repairsModel.findById(idDepot);
    if (!repair) {
      throw new Error("Aucune reparation en correspond a cet identifiant");
    }
    const { avancement } = req.body;
    repair.avancement = avancement;
    repair.save();

    res.status(statusCode.ok).json({
      message: "Avancement updated successfully",
    });
  } catch (error) {
    next(error);
  }
};

const bonDeSortieVoiture = async (req, res) => {};

module.exports = {
  receptionVoiture,
  allReparationsWithDetails,
  bonDeSortieVoiture,
  setAvancementRepairs,
};
