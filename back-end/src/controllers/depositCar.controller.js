const { role } = require("../globalVariables/role.values");
const { allCarsByUser, allCars } = require("../helpers/carDeposit.helper");

// fonction d'avoir toutes les voitures
const allDepositCar = async (req, res, next) => {
  try {
    const user = req.user;
    const allCarsDeposit =
      user.role == role.client
        ? await allCarsByUser(user._id)
        : await allCars();
    if (
      !allCarsDeposit &&
      typeof allCarsDeposit === Array &&
      allCarsDeposit.length === 0
    ) {
      throw new Error("Aucune n'a ete depose");
    }

    res.status(200).json({
      message: "All cars",
      data: allCarsDeposit,
    });
  } catch (error) {
    next(error);
  }
};

module.exports = {
  allDepositCar,
};
