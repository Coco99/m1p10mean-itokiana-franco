const statusCode = require("../globalVariables/statusCode");
const { depositCar, findCarById } = require("../helpers/carDeposit.helper");
const { recoveryCarById } = require("../helpers/recoveryCar.helper");
const repairsModel = require("../models/repairs.model");
const { role } = require("../globalVariables/role.values");
const { sendEmailText } = require("../helpers/sendEmail.helper");
const userModel = require("../models/user.model");

// fonction de deposer une voiture
const dropCar = async (req, res, next) => {
  try {
    const user = req.user;
    if (user.role !== role.client) {
      req.status = statusCode.notAllowRole;
      throw new Error("Utilisateur non authorise");
    }
    const carDetails = req.body;

    const car = await depositCar(user._id, carDetails);
    const allResponsable = await userModel.find({
      role: role.responsableAtelier,
    });
    console.log(allResponsable);

    allResponsable.forEach((resp) => {
      const mail = `Le client ${
        user.firstname + " " + user.name
      } a depose une voiture de modele ${carDetails.carModel}`;
      sendEmailText(resp.email, "Depot de voiture", mail);
    });

    res.status(200).json({
      message: `Car dropped off successfully by ${user.name}`,
      data: car,
    });
  } catch (error) {
    next(error);
  }
};

// fonction de recuperer une voiture
const recoveryCar = async (req, res, next) => {
  try {
    const user = req.user;
    if (user.role !== role.client) {
      req.status = statusCode.notAllowRole;
      throw new Error("Utilisateur non authorise");
    }
    const idCar = req.params.idCar;
    if (!idCar && idCar == "") {
      throw new Error("Aucune voitre n'a ete selectionner");
    }
    const depositCar = await findCarById(idCar);
    console.log(user._id, depositCar.owner._id);
    if (depositCar) {
      if (user._id == depositCar.owner._id) {
        const carRecovered = await recoveryCarById(depositCar._id);
        res.status(200).json({
          message: "Here's your car. Thanks for trusting us",
          data: {
            depositId: depositCar._id,
          },
        });
      } else {
        throw new Error("You are not the owner of this car");
      }
    } else {
      throw new Error("Car not deposit here");
    }
  } catch (error) {
    next(error);
  }
};

// fonction de recuperer tous les reparations en cours
const allRepairs = async (req, res, next) => {
  try {
    const user = req.user;
    if (user.role !== role.client) {
      req.status = statusCode.notAllowRole;
      throw new Error("Utilisateur non authorise");
    }
    const idCar = req.params.idCar;
    if (!idCar && idCar == "") {
      throw new Error("Aucune voitre n'a ete selectionner");
    }
    const depositCar = await findCarById(idCar);
    if (!depositCar) {
      throw new Error("Cette voiture est inexistant dans notre atelier");
    }
    if (depositCar.owner._id != user._id) {
      throw new Error("Cette voiture ne vous appartient pas");
    }
    const allRepairs = await repairsModel
      .find({ depositCar: idCar })
      .select({ prix: 0 })
      .exec();

    res.status(200).json({
      message: `All repair for the car: ${depositCar.carModel}`,
      data: {
        allRepairs,
      },
    });
  } catch (error) {
    next(error);
  }
};

// Fonction de recuperer tous les factures du client
const allBills = async (req, res) => {
  res.send("Toutes les factures du client");
};

// Historique des repartions sur une voiture
const repairsHistory = async (req, res) => {
  res.send(`Historique des reparations de la voiture ${req.params.idCar}`);
};

module.exports = {
  dropCar,
  recoveryCar,
  allRepairs,
  allBills,
  repairsHistory,
};
