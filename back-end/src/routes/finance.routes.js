const { Router } = require("express");
const {
  validerPaiements,
  tempsMoyenReparation,
  chiffreAffaire,
  benefices,
} = require("../controllers/finance.controller");
const financeRouter = Router();

financeRouter.get("/depotCar/:idDepot/validerPaiements", validerPaiements);
financeRouter.get("/statistics/repairs", tempsMoyenReparation);
financeRouter.get("/statistics/chiffreAffaire/:periode", chiffreAffaire);
financeRouter.post("/statistics/benefices", benefices);

module.exports = financeRouter;
