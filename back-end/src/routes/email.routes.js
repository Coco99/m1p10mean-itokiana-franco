const { Router } = require("express");
const { sendEmailHtml, sendEmailText } = require("../helpers/sendEmail.helper");
const emailRouter = Router();

// route pour envoyer un email
emailRouter.post("/send", async (req, res, next) => {
  try {
    const { to, subject, message } = req.body;
    sendEmailText(to, subject, message);
    res.status(200).json({
      message: "Email send successfully",
    });
  } catch (error) {
    next(error);
  }
});

module.exports = emailRouter;
