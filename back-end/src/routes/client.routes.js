const { Router } = require("express");
const { allDepositCar } = require("../controllers/depositCar.controller");
const {
  dropCar,
  recoveryCar,
  allRepairs,
  allBills,
  repairsHistory,
} = require("../controllers/client.controller");
const {
  getToken,
  authSignIn,
  authLogin,
  identity,
} = require("../globalVariables/controllerFn.global");
const { role } = require("../globalVariables/role.values");
const clientRouter = Router();

// Route pour se logger dans la partie du client
// Parametre {email, password}
// pas de role de specifier le role, il est defini automatiquement en back end
// retourne {message, data}
clientRouter.post("/login", [
  async (req, res, next) => {
    req.role = role.client;
    next();
  },
  authLogin,
]);

//Route pour s'inscrire en tant que client
//Parametre {firstname, name, email, password}
// pas besoin de specifier le role
//retourne {message, data: {token}}
clientRouter.post("/register", [
  async (req, res, next) => {
    req.role = role.client;
    next();
  },
  authSignIn,
]);

// route pour deposer une voiture
// il faut envoyer le token en Auth Bearer
// retourne {message, data: detailsDepot}
clientRouter.post("/cars/dropoff", [getToken, dropCar]);

// route pour voir toutes les voitures deposees
//il faut envoyer le token en Auth Bearer
// retourne {message, data: listCars}
clientRouter.get("/cars", [getToken, allDepositCar]);

// route pour recuperer une voiture
// il faut envoyer le token en Auth Bearer
// retourne {message, data {idDepot}}
clientRouter.get("/cars/recovery/:idCar", [getToken, recoveryCar]);

// route pour renvoyer toutes les reparations
// token en auth bearer
// retourne {message, allRepairs: [repairs]}
clientRouter.get("/cars/:idCar/repairs", [getToken, allRepairs]);

// route pour renvoyer toutes les factures
clientRouter.get("/bills", [getToken, allBills]);

// historique des reparations sur une voiture
clientRouter.get("/cars/:idCar/repairs/history", [getToken, repairsHistory]);

// Pour avoir les informations personnelles de l'utilisateur
clientRouter.get("/identity", [getToken, identity]);

module.exports = clientRouter;
