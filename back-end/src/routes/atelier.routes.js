const { Router } = require("express");
const { allDepositCar } = require("../controllers/depositCar.controller");
const {
  receptionVoiture,
  allReparationsWithDetails,
  bonDeSortieVoiture,
  setAvancementRepairs,
} = require("../controllers/atelier.controller");
const {
  getToken,
  authLogin,
  authSignIn,
} = require("../globalVariables/controllerFn.global");
const { role } = require("../globalVariables/role.values");
const atelierRouter = Router();

// Route pour se logger dans le cote du responsable atelier
// parametre {email, password}
// pas de besoin de specifier le role
// retourne {message, data: {token}}
atelierRouter.post("/login", [
  async (req, res, next) => {
    req.role = role.responsableAtelier;
    next();
  },
  authLogin,
]);

// Route pour s'inscrire dans le cote du responsable atelier
// parametre {firstname, name, email, password}
// pas besoin de specifier le role
// retourne {message, data: user}
atelierRouter.post("/register", [
  async (req, res, next) => {
    req.role = role.responsableAtelier;
    next();
  },
  authSignIn,
]);

// Route pour receptionner une voiture cote responsable atelier apres que le client depose sa voiture
// pas de formulaire mais parametre en GET {idDepot}
// Token a envoye en Auth Bearer
// retourne {message}
atelierRouter.get("/depotCar/:idDepot/reception", [getToken, receptionVoiture]);

// Route pour que le responsable atelier ajoute les reparations a une voiture
// pas de formulaire mais parametre en GET dans l'url {idDepot}
// token a envoye en Auth Bearer
// retourne {message}
atelierRouter.get("/depotCar/:idDepot/repairs", [
  getToken,
  allReparationsWithDetails,
]);

// Route pour avoir toutes les voitures deposées à l'atelier
// token a envoyé en Auth Bearer
// retourne {message, data: allDepositCar}
atelierRouter.get("/depotCar", [getToken, allDepositCar]);

// Route pour changer l'avancement d'une reparation
// Formulaire: {avancement}
// token a envoye en Auth Bearer
// retourne {message}
atelierRouter.post("/depotCar/:idDepot/repairs/:idRepairs", [
  getToken,
  setAvancementRepairs,
]);

atelierRouter.get("/depotCar/:idDepot/sortie", [getToken, bonDeSortieVoiture]);

module.exports = atelierRouter;
