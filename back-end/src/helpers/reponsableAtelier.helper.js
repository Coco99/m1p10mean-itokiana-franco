const depositCarModel = require("../models/carDeposit.model");
const { role } = require("../globalVariables/role.values");

const receptionCars = (idDepot, idReponsable) => {
  return depositCarModel.findByIdAndUpdate(idDepot, {
    $set: { responsableReception: idReponsable },
  });
};

module.exports = {
  receptionCars,
};
