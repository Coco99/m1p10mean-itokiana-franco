const recoveryCarModel = require("../models/recoveryCar.model");

const recoveryCarById = (idDeposit) => {
  return recoveryCarModel.create({
    carDeposit: idDeposit,
  });
};

module.exports = {
  recoveryCarById,
};
