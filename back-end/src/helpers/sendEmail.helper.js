const nodemailer = require("nodemailer");
const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: process.env.MAIL_USER,
    pass: process.env.MAIL_PASSWORD,
  },
});

const mailDefaultOption = {
  from: process.env.MAIL_USER,
};

const getMailOptions = (receiver, subject) => {
  return {
    ...mailDefaultOption,
    to: receiver,
    subject,
  };
};

const sendEmailText = (receiver, subject, text) => {
  const mailOptions = { ...getMailOptions(receiver, subject), text };
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) throw error;
    return info;
  });
};

const sendEmailHtml = (receiver, subject, body) => {
  const mailOptions = { ...getMailOptions(receiver, subject), html: body };
  let result = null;
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) throw error;
    result = info;
  });

  return result;
};

module.exports = {
  sendEmailText,
  sendEmailHtml,
};
