const carDepositModel = require("../models/carDeposit.model");

const depositCar = (ownerId, { carModel, carPicture }) => {
  return carDepositModel.create({
    owner: ownerId,
    carModel,
    carPicture,
  });
};

const allCars = () => {
  return carDepositModel.find().populate("owner");
};

const allCarsByUser = (ownerId) => {
  return carDepositModel.find({ owner: ownerId });
};

const findCarById = (carDepositId) => {
  return carDepositModel.findById(carDepositId).populate("owner").exec();
};

module.exports = {
  depositCar,
  allCars,
  allCarsByUser,
  findCarById,
};
