const mongoose = require("mongoose");
const uri = process.env.MONGO_URI;

const main = async (uri) => {
  try {
    mongoose.set("strictQuery", false);
    await mongoose.connect(uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log("Database connected");
  } catch (error) {
    throw error;
  }
};

main(uri);
