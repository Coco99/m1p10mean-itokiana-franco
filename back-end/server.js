const express = require("express");
require("dotenv").config();
require("./src/config/database/mongo.database");
const cors = require("cors");
const app = express();
const bodyParser = require("body-parser");
const clientRouter = require("./src/routes/client.routes");
const atelierRouter = require("./src/routes/atelier.routes");
const financeRouter = require("./src/routes/finance.routes");
const allRoutes = require("./src/globalVariables/allRoutes");
const emailRouter = require("./src/routes/email.routes");

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.json(allRoutes);
});

app.use("/client", clientRouter);
app.use("/atelier", atelierRouter);
app.use("/finance", financeRouter);
app.use("/email", emailRouter);

// En cas d'erreur dans les routes et les controllers
// Ce middleware est un gestion de tous les erreurs
app.use((error, req, res, next) => {
  res.status(req.status || 400).json({
    message: "Bad request",
    errorMessage: error.message,
  });
});
const port = process.env.SERVER_PORT || 3000;

app.listen(port, () => console.log(`Server is running in port ${port}`));
